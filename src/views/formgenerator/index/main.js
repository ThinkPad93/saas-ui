import Vue from 'vue'
import ElementUI from 'element-ui'
import '@/views/formgenerator/styles/index.scss'

import Home from './Home'
import router from '@/router/routers'
import i18n from '@/lang' // internationalization
import permission from '@/components/permission'

import store from '@/store'
import '@/router/index'

import '@/icons'
Vue.use(ElementUI, {
  i18n: (key, value) => i18n.t(key, value)
})
Vue.use(permission)
Vue.config.productionTip = false
new Vue({
  router,
  store,
  i18n,
  render: h => h(Home)
}).$mount('#app')
