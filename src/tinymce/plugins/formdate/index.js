// Exports the "formdate" plugin for usage with module loaders
// Usage:
//   CommonJS:
//     require('tinymce/plugins/formdate')
//   ES2015:
//     import 'tinymce/plugins/formdate'
require('./plugin.js');
