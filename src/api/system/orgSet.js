import request from '@/utils/request'

export function loadDataUrl() {
  return 'admin-service/api/orgSetMng/loadData'
}

export function deleteUrl() {
  return 'admin-service/api/orgSetMng/doDelete'
}

export function toAdd(data) {
  return request({
    url: 'admin-service/api/orgSetMng/toAdd',
    method: 'post',
    data
  })
}

export function toCopy(data) {
  return request({
    url: 'admin-service/api/orgSetMng/toCopy',
    method: 'post',
    data
  })
}

export function viewMainData(data) {
  return request({
    url: 'admin-service/api/orgSetMng/toView',
    method: 'post',
    data
  })
}

export function getOrgSetsByCpyCode(curOrgId) {
  const data = { id: curOrgId }
  return request({
    url: 'admin-service/api/orgSetMng/getOrgSetsByCpyCode',
    method: 'post',
    data
  })
}

export function addOrEdit(data) {
  return request({
    url: 'admin-service/api/orgSetMng/doSave',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: 'admin-service/api/orgSetMng/doDelete',
    method: 'post',
    data
  })
}

export function saveUserOrgSet(data) {
  return request({
    url: 'admin-service/api/orgSetMng/saveUserOrgSet',
    method: 'post',
    data
  })
}

export function deleteUserOrgSet(data) {
  return request({
    url: 'admin-service/api/orgSetMng/deleteUserOrgSet',
    method: 'post',
    data
  })
}

export function getOrgSetsByUserCpyCode() {
  return request({
    url: 'admin-service/api/orgSetMng/getOrgSetsByUserCpyCode',
    method: 'get'
  })
}
