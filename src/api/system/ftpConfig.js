import request from '@/utils/request'

export function loadDataUrl() {
  return 'admin-service/api/ftpConfigMng/loadData'
}

export function deleteUrl() {
  return 'admin-service/api/ftpConfigMng/doDelete'
}

export function toAdd(data) {
  return request({
    url: 'admin-service/api/ftpConfigMng/toAdd',
    method: 'post',
    data
  })
}

export function toCopy(data) {
  return request({
    url: 'admin-service/api/ftpConfigMng/toCopy',
    method: 'post',
    data
  })
}

export function viewMainData(data) {
  return request({
    url: 'admin-service/api/ftpConfigMng/toView',
    method: 'post',
    data
  })
}

export function addOrEdit(data) {
  return request({
    url: 'admin-service/api/ftpConfigMng/doSave',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: 'admin-service/api/ftpConfigMng/doDelete',
    method: 'post',
    data
  })
}

export function getAllFtpConfig() {
  return request({
    url: 'admin-service/api/ftpConfigMng/getAllFtpConfig',
    method: 'get'
  })
}
