import request from '@/utils/request'

export function loadDataUrl() {
  return 'admin-service/api/jobMng/loadData'
}

export function deleteUrl() {
  return 'admin-service/api/jobMng/doDelete'
}

export function toAdd(data) {
  return request({
    url: 'admin-service/api/jobMng/toAdd',
    method: 'post',
    data
  })
}

export function toCopy(data) {
  return request({
    url: 'admin-service/api/jobMng/toCopy',
    method: 'post',
    data
  })
}
export function viewMainData(data) {
  return request({
    url: 'admin-service/api/jobMng/toView',
    method: 'post',
    data
  })
}

export function addOrEdit(data) {
  return request({
    url: 'admin-service/api/jobMng/doSave',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: 'admin-service/api/jobMng/doDelete',
    method: 'post',
    data
  })
}

export function doPauseJob(id) {
  return request({
    url: 'admin-service/api/jobMng/pauseJob/' + id,
    method: 'put'
  })
}

export function doRunJob(id) {
  return request({
    url: 'admin-service/api/jobMng/runJob/' + id,
    method: 'put'
  })
}
