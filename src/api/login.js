import request from '@/utils/request'
import md5 from 'js-md5'

export function login(userAccount, password) {
  userAccount = userAccount.toLowerCase()
  return request({
    url: 'admin-service/api/userMng/login',
    method: 'post',
    data: {
      userAccount: userAccount,
      password: md5(password + userAccount)
    }
  })
}

export function logout() {
  return request({
    url: 'admin-service/api/userMng/logout',
    method: 'post'
    /* data: {
      userAccount
    } */
  })
}

export function getUserInfo() {
  return request({
    url: 'admin-service/api/userMng/info',
    method: 'get'
  })
}
