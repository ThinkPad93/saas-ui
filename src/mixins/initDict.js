import { getDictByType } from '@/api/system/dataDict'

export default {
  data() {
    return {
      dicts: {}
    }
  },
  methods: {
    async getDictByType(dictType) {
      return new Promise((resolve, reject) => {
        getDictByType(dictType).then(res => {
          this.dicts[dictType] = res.data
          resolve(res)
        }).catch(err => {
          reject(err)
        })
      })
    },
    // 同步结果 this.getSyncDictByType('cpy_status').then(res =>{ console.log(res,'return')})
    async getSyncDictByType(dictType) {
      const result = await new Promise((resolve, reject) => {
        getDictByType(dictType).then(res => {
          this.dicts[dictType] = res.data
          resolve(res)
        }).catch(err => {
          reject(err)
        })
      })
      return result
    }
  }
}
